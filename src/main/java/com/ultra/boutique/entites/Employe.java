package com.ultra.boutique.entites;

import java.time.LocalDate;
import java.util.Objects;

public class Employe extends Personne {
    
    private String cnss;
    private LocalDate dateEmbauche;

    public Employe(){}
    
    public Employe(String cnss, LocalDate dateEmbauche, int id, String nom, String prenom, LocalDate dateNaissance) {
        super(id, nom, prenom, dateNaissance);
        this.cnss = cnss;
        this.dateEmbauche = dateEmbauche;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), cnss, dateEmbauche);
    }

    @Override
    public String toString() {
        return "Employe{" +
                "cnss='" + cnss + '\'' +
                ", dateEmbauche=" + dateEmbauche +
                ", id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", dateNaissance=" + dateNaissance +
                '}';
    }
}
