package com.ultra.boutique.entites;

import java.time.LocalDate;
import java.util.Objects;

public class Client extends Personne{

    private String carteVisa;
    private String cin;

    public Client(){}
    
    public Client(String carteVisa,String cin, int id, String nom, String prenom, LocalDate dateNaissance) {
        super(id, nom, prenom, dateNaissance);
        this.carteVisa = carteVisa;
        this.cin=cin;
    }

    public String getCarteVisa() {
        return carteVisa;
    }

    public void setCarteVisa(String carteVisa) {
        this.carteVisa = carteVisa;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        return o instanceof Client && ((Client) o).id == this.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), carteVisa, cin);
    }

    @Override
    public String toString() {
        return "Client{" +
                "carteVisa='" + carteVisa + '\'' +
                ", cin='" + cin + '\'' +
                ", id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", dateNaissance=" + dateNaissance +
                '}';
    }
}
