package com.ultra.boutique.entites;

import java.time.LocalDate;
import java.util.Objects;

public class Personne {

    protected int id;
    protected String nom;
    protected String prenom;
    protected LocalDate dateNaissance;
    
    public Personne(){}
    
    public Personne(int id,String nom,String prenom,LocalDate dateNaissance){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
    }

    public int getAge(){
        return LocalDate.now().getYear() - this.dateNaissance.getYear();
    }
    
    public int getAge(LocalDate date) {
       return date.getYear() - this.dateNaissance.getYear();
    }
    
    public String getNom(){
        return this.nom;
    }
    
    public String getPrenom(){
        return this.prenom;
    }

    public void setNom(String nom){
        this.nom = nom;
    }
    public void setPrenom(String prenom){
        this.prenom = prenom;
    }
    public void setDateNaissance(LocalDate dateNaissance){
        this.dateNaissance = dateNaissance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        return o instanceof Personne && ((Personne) o).id == this.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom, dateNaissance);
    }

    @Override
    public String toString() {
        return "Personne{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", dateNaissance=" + dateNaissance +
                '}';
    }

    public int getId() {
        return this.id;
    }
}
