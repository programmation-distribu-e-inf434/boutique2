/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ultra.boutique.entites;

/**
 *
 * @author fless
 */
import java.util.Objects;

public class Categorie {

    private long id;
    private String libelle;
    private String description;

    public Categorie(){}
    
    public Categorie(long id, String libelle, String description) {
        this.id = id;
        this.libelle = libelle;
        this.description = description;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        return o instanceof Categorie && ((Categorie) o).id == this.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, libelle, description);
    }

    @Override
    public String toString() {
        return "Categorie{" +
                "libelle='" + libelle + '\'' +
                ", description='" + description + '\'' +
                ", id=" + id +
                '}';
    }

    public Long getId() {
        return this.id;
    }
}
