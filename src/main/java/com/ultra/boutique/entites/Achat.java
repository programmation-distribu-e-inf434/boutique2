package com.ultra.boutique.entites;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Achat {

    private long id;
    private double remise;
    private LocalDate dateAchat;
    private List<ProduitAchete> produitsAchetes = new ArrayList();

    public Achat(){}
    
    public Achat(long id, double remise, LocalDate dateAchat,ArrayList<ProduitAchete> produitsAchetes) {
        this.id = id;
        this.remise = remise;
        this.dateAchat = dateAchat;
        this.produitsAchetes = produitsAchetes;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public LocalDate getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(LocalDate dateAchat) {
        this.dateAchat = dateAchat;
    }

    public List<ProduitAchete> getProduitsAchetes(){
        return this.produitsAchetes;
    }
    
    public void setProduitsAchetes(){
        this.produitsAchetes = produitsAchetes;
    }
    
    public void effectuerAchat(ProduitAchete produit){
        this.produitsAchetes.add(produit);
    }
    
    public void annulerAchat(ProduitAchete produit){
        this.produitsAchetes.remove(produit);
    }
    
    public double getRemiseTotale(){
        double remiseT=0;
        for(ProduitAchete pa : produitsAchetes){
            remiseT+=pa.getRemise();
        }
        return remiseT+remise;
    }
    
    public double getPrixTotal(){
        double prix=0;
        for(ProduitAchete pa : produitsAchetes){
            prix+=pa.getPrixTotal();
        }
        return prix*(1-remise);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        return o instanceof Achat && ((Achat) o).id == this.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, remise, dateAchat, produitsAchetes);
    }

    @Override
    public String toString() {
        return "Achat{" +
                "remise=" + remise +
                ", dateAchat=" + dateAchat +
                ", produitsAchetes=" + produitsAchetes +
                ", id=" + id +
                '}';
    }

    public Long getId() {
        return this.id;
    }
}
