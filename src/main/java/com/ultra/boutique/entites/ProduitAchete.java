package com.ultra.boutique.entites;

import java.util.Objects;

public class ProduitAchete {

    private long id;
    private int quantite;
    private double remise;
    private Produit produit;

    public ProduitAchete(){}
    
    public ProduitAchete(long id, Produit produit,int quantite, double remise) {
        this.id = id;
        this.produit=produit;
        this.quantite = quantite;
        this.remise = remise;
    }

    public long getId() {
        return id;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }
    
    public double getPrixTotal(){
        return quantite*produit.getPrixUnitaire() - remise*quantite*produit.getPrixUnitaire();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ProduitAchete && ((ProduitAchete) o).id == this.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, quantite, remise, produit);
    }

    @Override
    public String toString() {
        return "ProduitAchete{" +
                "id=" + id +
                ", quantite=" + quantite +
                ", remise=" + remise +
                ", produit=" + produit +
                '}';
    }
}
