package com.ultra.boutique.entites;

import java.time.LocalDate;
import java.util.Objects;

public class Produit {

    private long id;
    private String libelle;
    private double prixUnitaire;
    private LocalDate datePeremption;
    private Categorie categorie;

    
    public Produit(){}
    
    public Produit(long id, String libelle, double prixUnitaire, LocalDate datePeremption, Categorie categorie) {
        this.id = id;
        this.libelle = libelle;
        this.prixUnitaire = prixUnitaire;
        this.datePeremption = datePeremption;
        this.categorie = categorie;
    }

    public long getId() {
        return id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public double getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public LocalDate getDatePeremption() {
        return datePeremption;
    }

    public void setDatePeremption(LocalDate datePeremption) {
        this.datePeremption = datePeremption;
    }
    
    public boolean estPerime(){
        return datePeremption.isBefore(LocalDate.now());
    }

    public boolean estPerime(LocalDate date){
        return datePeremption.isBefore(date);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        return o instanceof Produit && ((Produit) o).id == this.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, libelle, prixUnitaire, datePeremption, categorie);
    }
}
