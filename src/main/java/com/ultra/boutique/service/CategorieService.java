package com.ultra.boutique.service;

import com.ultra.boutique.entites.Categorie;

import java.util.List;

public class CategorieService extends Service<Categorie, Long> {

    @Override
    public Long getId(Categorie cat) {
        return cat.getId();
    }

    public CategorieService() {
        super();
    }

    public CategorieService(List<Categorie> liste) {
        super(liste);
    }
}
