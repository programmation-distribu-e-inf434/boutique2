package com.ultra.boutique.service;

import com.ultra.boutique.entites.Client;
import java.util.List;

public class ClientService extends Service<Client, Integer> {

    @Override
    public Integer getId(Client client) {
        return client.getId();
    }

    public ClientService() {
        super();
    }

    public ClientService(List<Client> liste) {
        super(liste);
    }
}


