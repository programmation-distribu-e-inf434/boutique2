package com.ultra.boutique.service;

import com.ultra.boutique.entites.Employe;

import java.util.List;

public class EmployeService extends Service<Employe, Integer> {

    @Override
    public Integer getId(Employe employe) {
        return employe.getId();
    }

    public EmployeService() {
        super();
    }

    public EmployeService(List<Employe> liste) {
        super(liste);
    }
}
