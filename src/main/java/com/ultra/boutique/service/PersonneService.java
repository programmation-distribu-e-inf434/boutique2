package com.ultra.boutique.service;

import com.ultra.boutique.entites.Personne;

import java.util.List;

public class PersonneService extends Service<Personne, Integer> {

    @Override
    public Integer getId(Personne personne) {
        return personne.getId();
    }

    public PersonneService(){
        super();
    }

    public PersonneService(List<Personne> liste) {
        super(liste);
    }
}
