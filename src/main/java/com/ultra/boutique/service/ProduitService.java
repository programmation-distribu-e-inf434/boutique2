package com.ultra.boutique.service;

import com.ultra.boutique.entites.Produit;

import java.util.List;

public class ProduitService extends Service<Produit, Long> {

    @Override
    public Long getId(Produit produit) {
        return produit.getId();
    }

    public ProduitService(){
        super();
    }

    public ProduitService(List<Produit> liste) {
        super(liste);
    }

}
