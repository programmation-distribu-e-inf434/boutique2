package com.ultra.boutique.service;

import java.util.ArrayList;
import java.util.List;

public abstract class Service<T, ID> {
    
    protected final List<T> liste;

    public abstract ID getId(T t);

    public Service() {
        liste = new ArrayList<T>();
    }

    public Service(List<T> liste){
        this.liste = liste;
    }
    
    public void ajouter(T t){
        this.liste.add(t);
    }
    
    public void modifier(T t){
        this.liste.set((Integer) getId(t), t);
    }
    
    public T trouver(int id){
        return liste.get(id);
    }
    
    public void supprimer(int id){
        this.liste.remove(liste.get(id));
    }
    
    public void supprimer(T t){
        this.liste.remove(t);
    }
    
    public List<T> lister(){
        return this.liste;
    }
    
    public List<T> lister(int debut, int nombre){
        return liste.subList(debut, nombre);
    }

}


