package com.ultra.boutique.service;

import com.ultra.boutique.entites.ProduitAchete;

import java.util.List;

public class ProduitAcheteService extends Service<ProduitAchete, Long> {

    @Override
    public Long getId(ProduitAchete produitAchete) {
        return produitAchete.getId();
    }

    public ProduitAcheteService(){
        super();
    }

    public ProduitAcheteService(List<ProduitAchete> liste){
        super(liste);
    }
}
