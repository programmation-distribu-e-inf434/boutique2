package com.ultra.boutique.service;

import com.ultra.boutique.entites.Achat;

import java.util.List;

public class AchatService extends Service<Achat, Long> {

    @Override
    public Long getId(Achat achat) {
        return achat.getId();
    }

    public AchatService(){
        super();
    }

    public AchatService(List<Achat> liste){
        super(liste);
    }
    
}

