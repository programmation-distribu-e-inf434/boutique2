package com.ultra.boutique.rs;

import com.ultra.boutique.entites.Employe;
import com.ultra.boutique.service.EmployeService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/employes")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EmployeRS {
    
    final EmployeService employeService = new EmployeService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employe> listerEmploye(){
        return employeService.lister();
    }

    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employe> listerEmploye(@QueryParam("start") int debut,@QueryParam("end") int nombre){
        return employeService.lister(debut, nombre);
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{employeId}")
    public Employe trouverEmploye(@PathParam("employeId") Integer employeId){
        return employeService.trouver(employeId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouterEmploye(Employe employe){
        employeService.ajouter(employe);
    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifierEmploye(Employe employe){
        employeService.modifier(employe);
    }

    @DELETE
    @Path("/{employeId}")
    public void supprimerEmploye(@PathParam("employeId")Integer employeId){
        employeService.supprimer(employeId);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimerEmploye(Employe employe){
        employeService.supprimer(employe);
    }

}
