package com.ultra.boutique.rs;

import com.ultra.boutique.entites.Client;
import com.ultra.boutique.service.ClientService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/clients")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ClientRS {
    
    final ClientService clientService = new ClientService();

    @GET
    public List<Client> listerClient(){
        return clientService.lister();
    }

    @GET
    @Path("/filtrer")
    public List<Client> listerClient(@QueryParam("start") int debut,@QueryParam("end") int nombre){
        return clientService.lister(debut, nombre);
    }


    @GET
    @Path("/{clientId}")
    public Client trouverClient(@PathParam("clientId") Integer clientId){
        return clientService.trouver(clientId);
    }

    @POST
    public void ajouterClient(Client client){
        clientService.ajouter(client);
    }


    @PUT
    public void modifierClient(Client client){
        clientService.modifier(client);
    }

    @DELETE
    @Path("/{clientId}")
    public void supprimerClient(@PathParam("clientId")Integer clientId){
        clientService.supprimer(clientId);
    }

    @DELETE
    public void supprimerClient(Client client){
        clientService.supprimer(client);
    }

}
