package com.ultra.boutique.rs;

import com.ultra.boutique.entites.Achat;
import com.ultra.boutique.service.AchatService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/achats")
public class AchatRS {
    
    final AchatService achatService = new AchatService();
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Achat> listerAchat(){
        return achatService.lister();
    }

    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Achat> listerAchat(@QueryParam("start") int debut,@QueryParam("end") int nombre){
        return achatService.lister(debut, nombre);
    }


    @GET
    @Path("/{achatId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Achat trouverAchat(@PathParam("achatId") Integer achatId){
        return achatService.trouver(achatId);
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouterAchat(Achat monAchat){
        achatService.ajouter(monAchat);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifierAchat(Achat achat){
        achatService.modifier(achat);
    }

    @DELETE
    @Path("/{achatId}")
    public void supprimerAchat(@PathParam("achatId")Integer achatId){
        achatService.supprimer(achatId);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimerAchat(Achat achat){
        achatService.supprimer(achat);
    }

}