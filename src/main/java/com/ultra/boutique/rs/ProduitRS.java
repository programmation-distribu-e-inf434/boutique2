package com.ultra.boutique.rs;



import com.ultra.boutique.entites.Produit;
import com.ultra.boutique.service.ProduitService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/produits")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProduitRS {
    
    final ProduitService produitService = new ProduitService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Produit> listerProduit(){
        return produitService.lister();
    }

    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Produit> listerProduit(@QueryParam("start") int debut,@QueryParam("end") int nombre){
        return produitService.lister(debut, nombre);
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{produitId}")
    public Produit trouverProduit(@PathParam("produitId") Integer produitId){
        return produitService.trouver(produitId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouterProduit(Produit produit){
        produitService.ajouter(produit);
    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifierProduit(Produit produit){
        produitService.modifier(produit);
    }

    @DELETE
    @Path("/{produitId}")
    public void supprimerProduit(@PathParam("produitId")Integer produitId){
        produitService.supprimer(produitId);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimerProduit(Produit produit){
        produitService.supprimer(produit);
    }
}
