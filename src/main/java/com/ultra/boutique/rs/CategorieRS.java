package com.ultra.boutique.rs;

import com.ultra.boutique.entites.Categorie;
import com.ultra.boutique.service.CategorieService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/categories")
public class CategorieRS {
    
    CategorieService categorieService = new CategorieService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Categorie> listerCategorie(){
        return categorieService.lister();
    }

    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Categorie> listerCategorie(@QueryParam("start") int debut,@QueryParam("end") int nombre){
        return categorieService.lister(debut, nombre);
    }


    @GET
    @Path("/{categorieId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Categorie trouverCategorie(@PathParam("categorieId") Integer categorieId){
        return categorieService.trouver(categorieId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouterCategorie(Categorie categorie){
        categorieService.ajouter(categorie);
    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifierCategorie(Categorie categorie){
        categorieService.modifier(categorie);
    }

    @DELETE
    @Path("/{categorieId}")
    public void supprimerCategorie(@PathParam("categorieId")Integer categorieId){
        categorieService.supprimer(categorieId);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimerCategorie(Categorie categorie){
        categorieService.supprimer(categorie);
    }
}
