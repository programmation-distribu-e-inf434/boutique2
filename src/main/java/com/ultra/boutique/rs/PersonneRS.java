package com.ultra.boutique.rs;

import com.ultra.boutique.entites.Personne;
import com.ultra.boutique.service.PersonneService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/personne")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PersonneRS {
    
    final PersonneService personneService = new PersonneService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Personne> listerPersonne(){
        return personneService.lister();
    }

    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Personne> listerPersonne(@QueryParam("start") int debut,@QueryParam("end") int nombre){
        return personneService.lister(debut, nombre);
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{personneId}")
    public Personne trouverPersonne(@PathParam("personneId") Integer personneId){
        return personneService.trouver(personneId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouterPersonne(Personne personne){
        personneService.ajouter(personne);
    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifierPersonne(Personne personne){
        personneService.modifier(personne);
    }

    @DELETE
    @Path("/{personneId}")
    public void supprimerPersonne(@PathParam("personneId")Integer personneId){
        personneService.supprimer(personneId);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimerPersonne(Personne personne){
        personneService.supprimer(personne);
    }
}
