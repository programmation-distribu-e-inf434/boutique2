package com.ultra.boutique.rs;

import com.ultra.boutique.entites.ProduitAchete;
import com.ultra.boutique.service.ProduitAcheteService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

public class ProduitAcheteRS {
    
    final ProduitAcheteService produitAcheteService = new ProduitAcheteService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProduitAchete> listerProduitAchete(){
        return produitAcheteService.lister();
    }

    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProduitAchete> listerProduitAchete(@QueryParam("start") int debut,@QueryParam("end") int nombre){
        return produitAcheteService.lister(debut, nombre);
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{produitAcheteId}")
    public ProduitAchete trouverProduitAchete(@PathParam("produitAcheteId") Integer produitAcheteId){
        return produitAcheteService.trouver(produitAcheteId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouterProduitAchete(ProduitAchete produitAchete){
        produitAcheteService.ajouter(produitAchete);
    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifierProduitAchete(ProduitAchete produitAchete){
        produitAcheteService.modifier(produitAchete);
    }

    @DELETE
    @Path("/{produitAcheteId}")
    public void supprimerProduitAchete(@PathParam("produitAcheteId")Integer produitAcheteId){
        produitAcheteService.supprimer(produitAcheteId);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimerProduitAchete(ProduitAchete produitAchete){
        produitAcheteService.supprimer(produitAchete);
    }
}
